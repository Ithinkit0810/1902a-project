import './App.css';
import React, { Component } from 'react';
import './styles/common.less';
import { dark, light } from './config/theme.config';
// @withRouter

// function Test(Com) {
//   return class extends Component {
//     render() {
//       return <Com age={123} />;
//     }
//   };
// }

// @Test
class App extends Component {
  state = {
    flag: true,
  };
  handleClickColor = () => {
    this.setState(
      {
        flag: !this.state.flag,
      },
      () => {
        window.less.modifyVars(this.state.flag ? light : dark);
      }
    );

    // flag:true  白色  false 黑色
  };
  render() {
    // console.log(this.props.age);
    const { flag } = this.state;
    return (
      <div className="container">
        <header>
          <button onClick={this.handleClickColor}>
            {flag ? '白色' : '黑色'}
          </button>
        </header>
        <section>
          <p className="grade">1902A</p>
          <p className="text">1902A66666</p>
        </section>
      </div>
    );
  }
}

export default App;
// export default App; withRouter(App)
